package golang

import (
	"fmt"
	"strings"

	"qlova.tech/ino"
)

type Compiler struct {
	ino.Tree
}

func (c *Compiler) Compile(tree ino.Tree) {
	if tree.
}

//HelloWorld is the i, Hello World.
const HelloWorld = `main: print("Hello World")`

func main() {
	ast, err := ino.FromReader(strings.NewReader(HelloWorld))
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(ast.Get("main"))
}
