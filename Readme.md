# inotation
Like json, inotation is a file format for serializing objects.

Specification:
```
//comment
//expressions can be 
//    "", 0-9...[a-z]..., _
//    [expression, expression, expression...], an alias,
//    {anonymous scope}, ?
//    action(expression, expression, expression...)

scope
    alias $= expression

    key: expression

    scope
        ...
    }
}
```



Examples:
(compare with json examples from https://json.org/example.html)
```
    glossary
        title: "example glossary"

        GlossDiv
            title: "S"

            GlossList
                ID: "SGML"
                SortAs: "SGML"
                GlossTerm: "Standard Generalized Markup Language"
                Acronym: "SGML"
                Abbrev: "ISO 8879:1986"
                para: ~
                    A meta-markup language, used to create markup 
                    languages such as DocBook.
                GlossSeeAlso: ["GML", "XML]
                GlossSee: "markup"
            }
        }
    }
```
```
    menu
        id: "file"
        value: "File"
        popup
            menuitem: [
                {value: "New",      onclick: CreateNewDoc()}
                {value: "Open",     onclick: OpenDoc()}
                {value: "Close",    onclick: CloseDoc()}
            ]
        }
    }
```
```
    widget
        debug: "on",
        window
            title: "Sample Konfabulator Widget"
            name: "main_window",
            width: 500
            height: 500
        }
        image
            src: "Images/Sun.png"
            name: "sun1"
            hOffset: 250
            vOffset: 250
            alignment: "center"
        }
        text
            data: "Click here"
            size: 36
            style: "bold"
            name: "text1"
            hOffset: 250
            vOffset: 100
            alignment: "center"
            onMouseUp: ~
                sun1.opacity = (sun1.opacity / 100) * 90;
        }
    }
```