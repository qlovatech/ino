package ino

import (
	"strconv"
	"strings"
)

//Tree is the basic construct in inotation.
type Tree struct {
	value

	key   string
	Value Value

	parent *Tree

	aliases map[string]Value

	sequence []*Tree
	mappings map[string]*Tree
}

func treeFromValue(parent *Tree, key string, expr Value) *Tree {
	var tree = &Tree{parent: parent, key: key, Value: expr}
	if subtree, ok := expr.(*Tree); ok {
		tree = subtree
		tree.parent = parent
		tree.key = key
	}
	return tree
}

func (tree *Tree) insert(key string, value Value) {
	if tree.mappings == nil {
		tree.mappings = make(map[string]*Tree)
	}
	var child = treeFromValue(tree, key, value)
	tree.mappings[key] = child
	tree.sequence = append(tree.sequence, child)
}

//Get returns the Tree at the given key.
func (tree *Tree) Get(key string) *Tree {
	if strings.Contains(key, ".") {
		path := strings.Split(key, ".")
		for _, key := range path {
			tree = tree.mappings[key]
		}
		return tree
	}
	return tree.mappings[key]
}

//Index returns the tree at the given index.
func (tree *Tree) Index(i int) *Tree {
	if l, ok := tree.Value.(*List); ok {
		if i < 0 || i >= len(l.Items) {
			return &Tree{}
		}
		return treeFromValue(tree, strconv.Itoa(i), l.Items[i])
	}

	if i < 0 || i >= len(tree.sequence) {
		return &Tree{}
	}
	return tree.sequence[i]
}

//String returns the current value as a string.
func (tree Tree) String() string {
	return tree.Value.String()
}
