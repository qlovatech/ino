package ino

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strings"
)

var Trace = os.Getenv("TRACE") != ""
var Panic = os.Getenv("PANIC") != ""
var Counter = 2

type Key = string

type Validator interface {
	Validate(Tree, Key, Value) error
}

type nilValidator struct{}

func (nilValidator) Validate(Tree, Key, Value) error {
	return nil
}

//Decoder is an ino decoder.
type Decoder struct {
	Validator

	reader *bufio.Reader

	Directory          string
	LastLine           []byte
	Line               []byte
	LineNumber, Column int
	Filename           string

	nextByte byte
}

//NewDecoder returns a new ino.Decoder.
func NewDecoder(reader io.Reader) *Decoder {
	return &Decoder{reader: bufio.NewReader(reader)}
}

func (d *Decoder) scanTree() (tree *Tree, err error) {
	tree = new(Tree)
	for {
		token, err := d.scanToken()
		if err != nil {
			if err == io.EOF {
				return tree, nil
			}
			return tree, err
		}

		if token == "\n" {
			continue
		}

		if token == "}" {
			return tree, nil
		}

		next, err := d.scanToken()
		if err != nil {
			if err == io.EOF {
				return tree, d.error("unexpected end of file")
			}
			return tree, err
		}

		switch next {
		case ":":
			expression, err := d.scanExpression()
			if err != nil {
				if err == io.EOF {
					return tree, d.error("unexpected end of file scanning expression")
				}
				return tree, err
			}
			tree.insert(token, expression)
		case "\n":
			subtree, err := d.scanTree()
			if err != nil {
				if err == io.EOF {
					return tree, d.error("unexpected end of file")
				}
				return tree, err
			}
			tree.insert(token, subtree)
		default:
			return &Tree{}, d.error("unimplemented %v, %v", token, next)
		}
	}
}

//Decode decodes into the provided interface and returns a ino.Tree.
func (d *Decoder) Decode(into interface{}) (tree *Tree, err error) {
	if d.Validator == nil {
		d.Validator = nilValidator{}
	}

	return d.scanTree()
}

func (d *Decoder) peekByte() byte {
	if d.nextByte != 0 {
		return d.nextByte
	}

	var b, err = d.reader.Peek(1)
	if err != nil {
		return 0
	}
	return b[0]
}

func (d *Decoder) readByteRaw(ignorespace bool) (byte, error) {
	if d.nextByte != 0 {
		b := d.nextByte
		d.nextByte = 0
		return b, nil
	}

	//Record line numbers, character position and the last line.
	b, err := d.reader.ReadByte()

	//Line numbers should always start at one.
	if d.LineNumber == 0 {
		d.LineNumber = 1
	}

	if b != '\t' || ignorespace {
		d.Column++
		d.Line = append(d.Line, b)
	}
	if b == '\n' && !ignorespace {
		d.Column = 0
		d.LineNumber++
		d.LastLine = d.Line
		d.Line = nil
	}

	return b, err
}

func (d *Decoder) readByte() (byte, error) {
	return d.readByteRaw(false)
}

func (d *Decoder) unreadByte() {
	if len(d.Line) == 0 {
		d.nextByte = d.LastLine[len(d.LastLine)-1]
		return
	}
	d.nextByte = d.Line[len(d.Line)-1]
}

func (d *Decoder) readBytes(symbol byte) ([]byte, error) {
	var result []byte

	for {
		var b = d.peekByte()

		if _, err := d.readByte(); err != nil {
			return nil, err
		}

		result = append(result, b)
		if b == symbol {
			break
		}

	}

	return result, nil
}

func (d *Decoder) scanToken() (string, error) {
	var token []byte
	for b := d.peekByte(); true; b = d.peekByte() {

		switch b {
		case ':', '$', '=', '+', '-', '*', '%', '@', '/', '(', ')', '\n', '[', ']', ',':
			if len(token) > 0 {
				return string(token), nil
			} else {
				if _, err := d.readByte(); err != nil {
					return "", err
				}
				return string(rune(b)), nil
			}
		case '"':
			if _, err := d.readByte(); err != nil {
				return "", err
			}

			s, err := d.readBytes('"')
			if err != nil {
				return "", err
			}

			return string(rune(b)) + string(s), nil

		case ' ', '\t':
			if _, err := d.readByte(); err != nil {
				return "", err
			}
			continue
		default:
			if _, err := d.readByte(); err != nil {
				return "", err
			}
			token = append(token, b)
		}

	}
	return "", d.error("invalid")
}

func (d *Decoder) scanNewLine() error {
	b, err := d.readByte()
	if err != nil {
		if err == io.EOF {
			return nil
		}
		return err
	}

	if b != '\n' && b != ',' {
		return d.error("expecting newline, got %v", string(rune(b)))
	}

	return nil
}

type Error struct {
	Formatted string
	Message   string
}

func (err Error) Error() string {
	return err.Formatted
}

func (d *Decoder) error(format string, args ...interface{}) error {
	var msg = fmt.Sprintf(format, args...)

	var wdir string
	if runtime.GOOS != "js" {
		wdir, _ = os.Getwd()
	}

	var rpath, _ = filepath.Rel(wdir, d.Directory)

	if len(rpath) > len(d.Directory) {
		rpath = d.Directory
	}

	var RestOfTheLine []byte

	if d.Column == 0 {
		RestOfTheLine = d.LastLine
		d.Column = len(d.LastLine)
	} else {
		RestOfTheLine, _ = d.reader.ReadBytes('\n')
	}

	var formatted = fmt.Sprint(rpath, d.Filename, ":",
		d.LineNumber, ": ", string(d.Line), string(RestOfTheLine), "\n",
		strings.Repeat(" ", d.Column+3+len(rpath)+len(d.Filename)), "^\n", msg)

	if Trace {
		var stacktrace = debug.Stack()
		var reader = bufio.NewReader(bytes.NewBuffer(stacktrace))

		const count = 7
		for i := 0; i < count; i++ {
			line, err := reader.ReadString('\n')
			if err != nil {
				formatted += string(stacktrace)
				break
			}
			if i == count-1 {
				formatted += "\n(" + strings.TrimSpace(line) + ")\n"
			}
		}

	}

	if Panic {
		Counter--
		if Counter == 0 {
			panic(formatted)
		}
	}

	return Error{formatted, msg}
}
