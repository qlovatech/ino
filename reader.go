package ino

import (
	"io"
	"os"
)

//Open uses os.Open to parse a ino.Tree.
func Open(name string) (*Tree, error) {
	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	return FromReader(f)
}

//FromReader returns an ino.Tree from the given reader.
func FromReader(reader io.Reader) (*Tree, error) {
	return NewDecoder(reader).Decode(nil)
}
