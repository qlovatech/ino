package ino

import (
	"fmt"
	"strconv"
	"strings"
)

type value struct {
	Line       []byte
	Column     int
	LineNumber int
	Filename   string
}

func (v *value) setLine(d *Decoder) {
	v.Line = d.Line
	v.Column = d.Column
	v.LineNumber = d.LineNumber
	v.Filename = d.Filename
}

//Value is a valid inotation value.
type Value interface {
	String() string
	setLine(*Decoder)
}

//Action is a type of Expression
type Action struct {
	value
	Name string
	Args []Value
}

func (a Action) String() string {
	var args = "["
	for i, expr := range a.Args {
		args += expr.String()
		if i < len(a.Args)-1 {
			args += ","
		}
	}
	return fmt.Sprintf("%v(%v)", a.Name, args+"]")
}

//String is an expression that contains no spaces and does not start with a numerical character.
type String struct {
	value
	Data string
}

func (s String) String() string {
	return s.Data
}

//List is a type of Expression that contains an ordered sequence of values.
type List struct {
	value
	Items []Value
}

func (l List) String() string {
	var result = "["
	for i, expr := range l.Items {
		result += expr.String()
		if i < len(l.Items)-1 {
			result += ","
		}
	}
	return result + "]"
}

//Numerical is a type of Expression that contains either digits or symbols, it may end with any character.
type Numerical struct {
	value
	Data string
}

func (n Numerical) String() string {
	return string(n.Data)
}

//Empty is a type of Expression
type Empty struct {
	value
}

func (Empty) String() string {
	return string("_")
}

func (d *Decoder) scanExpression() (Value, error) {
	expr, err := d.expression()
	if err != nil {
		return expr, err
	}

	expr, err = d.shunt(expr, 0)
	if err != nil {
		return expr, err
	}

	if err = d.scanNewLine(); err != nil {
		return expr, err
	}

	return expr, err
}

func (d *Decoder) scanValue() (Value, error) {
	expr, err := d.expression()
	if err != nil {
		return expr, err
	}

	expr, err = d.shunt(expr, 0)
	if err != nil {
		return expr, err
	}

	return expr, err
}

func (d *Decoder) expression() (Value, error) {
	token, err := d.scanToken()
	if err != nil {
		return nil, err
	}

	if len(token) == 0 {
		return nil, nil
	}

	if strings.HasPrefix(token, `"`) {
		s, err := strconv.Unquote(token)
		if err != nil {
			return nil, d.error("could not parse string %v: %w", token, err)
		}
		return &String{Data: s}, err
	}

	switch token[0] {
	case '-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		return &Numerical{Data: token}, nil

	case '{':
		return d.scanTree()

	case '[':
		var list List
		for {
			if d.peekByte() == ']' {
				d.readByte()
				break
			}

			expression, err := d.scanValue()
			if err != nil {
				return nil, err
			}

			list.Items = append(list.Items, expression)
		}
		return &list, nil
	}

	if d.peekByte() == '(' {
		d.readByte()

		var action Action
		action.Name = token
		for {
			if d.peekByte() == ')' {
				d.readByte()
				break
			}

			expression, err := d.scanValue()
			if err != nil {
				return nil, err
			}

			action.Args = append(action.Args, expression)
		}
		return &action, nil
	}

	return nil, d.error("unrecognized expression %v", token)
}
