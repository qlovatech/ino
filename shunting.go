package ino

//Precedence returns i's precedence for the specified symbol.
func Precedence(symbol string) int {
	if symbol == "" {
		return -1
	}
	switch string(symbol) {
	case ",", ")", "]", "\n", "", "}", "in", ":", ";", "to":
		return -1

	case "|":
		return 0

	case "&":
		return 1

	case "=", "<", ">", "!":
		return 2

	case "+", "-":
		return 3

	case "*", "/", `%`:
		return 4

	case "^":
		return 5

	case "(", "[":
		return 6

	case ".":
		return 7

	default:
		return -2
	}
}

func (d *Decoder) shunt(exp Value, precedence int) (Value, error) {
	//shunting:
	for symbol, err := d.scanToken(); Precedence(symbol) >= precedence; {
		if err != nil {
			return nil, err
		}

		current := symbol

		rhs, err := d.expression()
		if err != nil {
			return exp, err
		}

		symbol, err = d.scanToken()
		if err != nil {
			return nil, err
		}

		for Precedence(symbol) > Precedence(current) {
			rhs, err = d.shunt(rhs, Precedence(symbol))
			if err != nil {
				return exp, err
			}

			symbol, err = d.scanToken()
			if err != nil {
				return nil, err
			}
		}

		//Numerical shunting.
		if n, ok := exp.(*Numerical); ok {
			exp = &Numerical{Data: n.Data + symbol + rhs.(*Numerical).Data}
			continue
		}

		return nil, d.error("Operator "+string(symbol)+" does not apply to %v", exp)
	}

	d.unreadByte()
	return exp, nil
}
