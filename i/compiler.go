package main

import (
	"errors"
	"fmt"
	"strings"

	"qlova.tech/ino"
)

//HelloWorld is the i, Hello World.
const HelloWorld = `main: echo("Hello World")`

//Compiler compiles i syntax.
type Compiler struct{}

//CompileTree compiles a tree.
func (i *Compiler) CompileTree(tree *ino.Tree) error {
	switch val := tree.Value.(type) {
	case *ino.Action:
		return i.CompileAction(val)
	default:
		return errors.New("Unimplimented")
	}
	return nil
}

//CompileAction compiles an ino.Action.
func (i *Compiler) CompileAction(action *ino.Action) error {
	switch action.Name {
	case "print":
		fmt.Println(action.Args)
	default:
		return errors.New("Unknown concept " + action.Name)
	}
	return nil
}

func main() {
	decoder := ino.NewDecoder(strings.NewReader(HelloWorld))
	decoder.Filename = "HelloWorld"

	ast, err := decoder.Decode(nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	var i Compiler
	err = i.CompileTree(ast.Get("main"))
	if err != nil {
		fmt.Println(err)
		return
	}
}
